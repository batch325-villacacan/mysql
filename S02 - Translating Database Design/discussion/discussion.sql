-- SQL syntaxes are not case sensitive.
-- If you are going to add a SQL syntax(capitalize).
-- If you are going to use or add name of the column or table, we use lowercase.

-- To show all the list of our databases:
SHOW DATABASES;

-- TO create or add a database:
CREATE DATABASE music_db;

--TO drop or delete a database:
DROP DATABASE music_db;

-- Select a database:
USE music_db;

-- Show table inside a database
SHOW TABLES;

-- Creating a table:
CREATE TABLE users(
    id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL,
    full_name VARCHAR(50) NOT NULL,
    contact_number INT NOT NULL,
    email VARCHAR(50),
    address VARCHAR(50),
    PRIMARY KEY (id)
);

-- Mini-activity: You are going to create artist' table:
CREATE TABLE artists(
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    PRIMARY KEY (id)
);

-- TO change the name of the Table;
ALTER TABLE table_to_be_edited RENAME updated_name;
RENAME TABLE table_to_be_edited TO updated_name;

CREATE TABLE albums(
    id INT NOT NULL AUTO_INCREMENT,
    album_title VARCHAR(50) NOT NULL,
    date_released DATE NOT NULL,
    artist_id INT NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT fk_albums_artist_id
            FOREIGN KEY (artist_id) REFERENCES artists(id)
            ON UPDATE CASCADE
            ON DELETE RESTRICT
);

-- 2
-- ON UPDATE CASCADE para magupdate yung id
-- ON DELETE RESTRICT para di madedelete

CREATE TABLE songs(
    id INT NOT NULL AUTO_INCREMENT,
    song_title VARCHAR(50) NOT NULL,
    song_length TIME NOT NULL,
    song_genre VARCHAR(50) NOT NULL,
    album_id INT NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT fk_songs_album_id
        FOREIGN KEY (album_id) REFERENCES albums(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

CREATE TABLE playlists (
    id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    datetime_created DATETIME NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT fk_playlists_users_id
        FOREIGN KEY(user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);

-- Multiple Foreign Key in one table.
CREATE TABLE playlist_songs(
    id INT NOT NULL AUTO_INCREMENT,
    playlist_id INT NOT NULL,
    song_id INT NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT fk_playlist_songs_playlist_id
        FOREIGN KEY (playlist_id) REFERENCES playlists(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
    CONSTRAINT fk_playlist_songs_song_id
    FOREIGN KEY (song_id) REFERENCES songs(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);