-- [SECTIONS] INSERT records(CREATE);

--INSERT INTO name_of_table (columns_on_table) VALUES (values_per_column);

INSERT INTO artists(name) VALUES  ('Rivermaya');
INSERT INTO artists(name) VALUES  ('Psy');

DESCRIBE artists;
DESCRIBE playlist_songs;

INSERT INTO albums(album_title, date_released, artist_id) VALUES ('Psy 6', '2012-1-1', 2);
INSERT INTO albums(album_title, date_released, artist_id) VALUES ('Trip', '1996-1-1', 1);

-- Renaming column
ALTER TABLE table_name RENAME COLUMN old_name TO new_name;

INSERT INTO songs(song_title, song_length, song_genre, album_id) VALUES ('Gangnam Style', 253, 'K-pop', 1);
-- 253 -> 2:53

INSERT INTO songs(song_title, song_length, song_genre, album_id) VALUES ('Kundiman', 234, 'OPM', 2);

-- Change data type
ALTER TABLE [tbl_name] ALTER COLUMN [col_name_1] [DATA_TYPE]
ALTER TABLE songs ALTER COLUMN song_genre VARCHAR;

SELECT * FROM songs WHERE genere = 'OPM';
SELECT song_name, song_length FROM songs WHERE genere = 'OPM';

-- AND and OR keyword
SELECT * FROM songs WHERE song_length > 240 AND song_genre = 'OPM';

-- [SECTION] UPDATING RECORDS
-- UPDATE table_name SET column_name = value WHERE condition;

UPDATE songs SET song_length = 240;

DELETE FROM songs WHERE genre = 'OPM' AND song_length   

-- [SECTION] UPDATING RECORDS
-- UPDATE table_name SET column_name = value WHERE condition;

UPDATE songs SET length = 240;
UPDATE songs SET length = 259 WHERE id = 2;

UPDATE songs SET length = 300 WHERE length > 240 AND genre = 'OPM';

-- [SECTION] DELETE Record/s
-- DELETE FROM table_name WHERE condtion;
DELETE FROM songs WHERE genre = "OPM" AND length > 240;
