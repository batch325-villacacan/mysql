-- Add 5 artists;
INSERT INTO artists (name) VALUES ("Taylor Swift");
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");

-- Add album to TS:
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Fearless", "2008-1-1", 3);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Red", "2012-1-1", 3);

INSERT INTO songs (song_title, song_length, song_genre, album_id) VALUES ("Fearless", 246, "Pop rock", 101);

INSERT INTO songs (song_title, song_length, song_genre, album_id) VALUES ("Love Story", 213, "Country pop", 102);

INSERT INTO songs (song_title, song_length, song_genre, album_id) VALUES ("State of Grace", 253, "Rock, alternative rock, arena rock", 101);

INSERT INTO songs (song_title, song_length, song_genre, album_id) VALUES ("Red", 204, "Country", 102);

-- Lady Gaga;
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("A Star Is Born", "2018-1-1", 4);

-- A star is born
INSERT INTO songs (song_title, song_length, song_genre, album_id) VALUES ("Black Eyes", 151, "Rock and roll", 103);

INSERT INTO songs (song_title, song_length, song_genre, album_id) VALUES ("Shallow", 201, "Country, rock, folk rock", 103);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Born This Way", "2011-1-1", 4);

-- [SELECT] ADVANCED Selects;

-- to exclude record
SELECT * FROM songs WHERE id !=11;
SELECT song_name, genre FROM songs WHERE id !=11;

-- greater than or equal operator
SELECT * FROM songs WHERE id >= 11;

-- less than or equal operator
SELECT * FROM songs WHERE id <= 11;

-- id from 5 to 11
SELECT * FROM songs WHERE id >=5 AND id <= 11;
SELECT * FROM songs WHERE id BETWEEN 5 AND 11;

-- get specific IDs
SELECT * FROM songs WHERE id = 11;

-- OR Operator
SELECT * FROM songs WHERE id = 1 OR id = 11 OR id = 5;

-- IN operator
SELECT * FROM songs WHERE id IN (1,11,5);

-- Find partial matches:

-- ends in a
SELECT * FROM songs WHERE song_name LIKE "%a";
-- ends in e
SELECT * FROM songs WHERE song_name LIKE "%e";
-- starts with a
SELECT * FROM songs WHERE song_name LIKE "s%";
-- in between or all with
SELECT * FROM songs WHERE song_name LIKE "%s%";


SELECT * FROM songs WHERE album_id LIKE "%10%";
SELECT * FROM songs WHERE album_id LIKE "10_";

-- SORT record ascending
SELECT * FROM songs ORDER BY song_name ASC;

-- SORT record descending
SELECT * FROM songs ORDER BY song_name DESC;

-- DISTINCT records: no duplicate
SELECT DISTINCT genre  FROM songs;

-- [SECTION] JOINING TABLES
SELECT * FROM artists JOIN albums ON artists.id = albums.artist_id;

-- JOIN albums table and songs table:
SELECT * FROM albums JOIN songs ON albums.id = songs.album_id;

SELECT albums.album_title, songs.song_name, songs.length FROM albums JOIN songs ON albums.id = songs.album_id;

SELECT * FROM artists 
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;

SELECT artists.name, albums.album_title, songs.song_name FROM artists 
	JOIN albums ON artists.id = albums.artist_id
	JOIN songs ON albums.id = songs.album_id;